import { Schema, Document, model } from 'mongoose';
import { environment } from '../../environments/environment';

let companySchema = new Schema({
    name: {
        type: String,
        required: [true, 'The name is necessary'],
        unique: true
    },
    state: {
        type: Boolean,
        default: true
    },
    description: {
        type: String,
        required: [true, 'The description is necessary']
    },
    image: {
        type: String,
        default: `${environment.HOST}uploads/company/image/defaultCompany.png`
    }
},
{
    timestamps: true
});

interface ICompany extends Document {
    name: string;
    state: boolean;
    description: string;
    image: string
}

export default model<ICompany>('Company', companySchema);