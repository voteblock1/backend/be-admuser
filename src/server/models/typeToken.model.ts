import { Schema, Document, model } from 'mongoose';

let typeTokenSchema = new Schema({
    name: {
        type: String,
        required: [true, 'The name is necessary'],
        unique: true
    },
    state: {
        type: Boolean,
        default: true
    }
},
{
    timestamps: true
});

interface ITypeToken extends Document {
    name: string;
    state: boolean;
}

export default model<ITypeToken>('TypeToken', typeTokenSchema);