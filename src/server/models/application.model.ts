import { Schema, Document, model } from 'mongoose';

let applicationSchema = new Schema({
    name: {
        type: String,
        required: [true, 'The name is necessary'],
        unique: true
    },
    clientSecret: {
        type: String,
        required: [true, 'The clientSecret is necessary'],
        unique: true
    },
    state: {
        type: Boolean,
        default: true
    },
    idCompany: {
        type: Schema.Types.ObjectId, 
        ref: 'Company', required: true
    },
    idTypeUser: {
        type: Schema.Types.ObjectId, 
        ref: 'TypeUser', required: true
    }
},
{
    timestamps: true
});

// applicationSchema.methods.toJSON = function (){
//     let application = this;
//     let userObject = application.toObject();
//     delete userObject.clientSecret;

//     return userObject;
// }

interface IApplication extends Document {
    name: string;
    clientSecret: string;
    state: boolean;
    idCompany: string;
    idTypeUser: string;
}

export default model<IApplication>('Application', applicationSchema);