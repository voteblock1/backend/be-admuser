import { Schema, Document, model } from 'mongoose';
import { environment } from '../../environments/environment';

let userSchema = new Schema({
    firstName: {
        type: String,
        required: [true, 'The firstName is necessary']
    },
    lastName: {
        type: String,
        required: [true, 'The lastName is necessary']
    },
    email: {
        type: String,
        unique: [true,'Email it must be unique'],
        required: [true, 'Email is necessary']
    },
    password: {
        type: String,
        required: [true, 'Password is required']
    },
    state: {
        type: Boolean,
        default: true
    },
    idTypeUser: { 
        type: Schema.Types.ObjectId, 
        ref: 'TypeUser', required: false
    },
    checkEmail: {
        type: Boolean,
        default: false
    },
    image: {
        type: String,
        required: false,
        default: `${environment.HOST}uploads/user/image/defaultUser.png`
    },
    idCompany: {
        type: Schema.Types.ObjectId, 
        ref: 'Company', required: true
    }
},
{
    timestamps: true
});

userSchema.methods.toJSON = function (){
    let user = this;
    let userObject = user.toObject();
    delete userObject.password;

    return userObject;
}

interface IUser extends Document {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    state: boolean;
    idTypeUser: string;
    checkEmail: boolean;
    image: string;
    idCompany: string;
}

export default model<IUser>('User', userSchema);