import { Schema, Document, model } from 'mongoose';

let tokenEmailSchema = new Schema({
    userId: { 
        type: Schema.Types.ObjectId, 
        ref: 'User', 
        required: true
    },
    token: {
        type: String,
        required: [true, 'The token is necessary'],
    },
    state: {
        type: Boolean,
        default: true
    },
    createdAt: {
        type: Date,
        default: Date.now,
        expires: 43200
    },
    idTypeToken: {
        type: Schema.Types.ObjectId, 
        ref: 'TypeToken', 
        required: [true, 'The idTypeToken is necessary']
    }
});

interface IToken extends Document {
    userId: string;
    token: string;
    state: boolean;
    createdAt: Date;
    idTypeToken: string;
}

export default model<IToken>('Token', tokenEmailSchema);