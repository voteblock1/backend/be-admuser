import { Schema, Document, model } from 'mongoose';

let voterSchema = new Schema({
    userId: { 
        type: Schema.Types.ObjectId, 
        ref: 'User', 
        required: true
    },
    gradeId: {
        type: Schema.Types.ObjectId, 
        ref: 'Grade', 
        required: true
    },
    codeStudent: {
        type: String,
        required: [true, 'The codeStudent is necessary'],
    },
    codeID: {
        type: String,
        required: [true, 'The codeID is necessary'],
    },
    state: {
        type: Boolean,
        default: true
    }
},
{
    timestamps: true
});

voterSchema.methods.toJSON = function (){
    let voter = this;
    let userObject = voter.toObject();
    delete userObject.codeID;

    return userObject;
}

interface IVoter extends Document {
    userId: string;
    gradeId: string;
    codeStudent: string;
    codeID: string;
    state: boolean;
}

export default model<IVoter>('Voter', voterSchema);