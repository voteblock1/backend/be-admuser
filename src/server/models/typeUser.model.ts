import { Schema, Document, model } from 'mongoose';

let typeUsersSchema = new Schema({
    name: {
        type: String,
        required: [true, 'The name is necessary'],
        unique: true
    },
    description: {
        type: String,
        required: [true, 'The description is necessary']
    },
    state: {
        type: Boolean,
        default: true
    }
},
{
    timestamps: true
});

interface ITypeUser extends Document {
    name: string;
    description: string;
    state: boolean;
}

export default model<ITypeUser>('TypeUser', typeUsersSchema);