import { Schema, Document, model } from 'mongoose';

let gradeSchema = new Schema({
    name: {
        type: String,
        required: [true, 'The grade is necessary'],
    },
    state: {
        type: Boolean,
        default: true
    }
},
{
    timestamps: true
});

interface IGrade extends Document {
    name: string;
    state: boolean;
}

export default model<IGrade>('Grade', gradeSchema);