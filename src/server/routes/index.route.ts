import express = require('express');
import controllerTypeUser from '../controllers/typeUser.controller';
import controllerUser from '../controllers/user.controller';
import controllerCompany from '../controllers/company.controller';
import controllerApplication from '../controllers/application.controller';
import controllerAuth from '../controllers/auth.controller';
import controllerLoginUser from '../controllers/loginUser.controller';
import controllerTypeToken from '../controllers/typeToken.controller';
import controllerToken from '../controllers/token.controller';
import controllerVoter from '../controllers/voter.controller';
import controllerGrade from '../controllers/grade.controller';
import controllerLoginVoter from '../controllers/loginVoter.controller';


export default class Routes {
    private app: express.Application;

    constructor(){
        this.app = express();
        this.loadRoutes();
    }

    private loadRoutes(): void {
        this.app.use( controllerTypeUser );
        this.app.use( controllerUser );
        this.app.use( controllerCompany );
        this.app.use( controllerApplication );
        this.app.use( controllerAuth );
        this.app.use( controllerLoginUser );
        this.app.use( controllerTypeToken );
        this.app.use( controllerToken );
        this.app.use( controllerVoter );
        this.app.use( controllerGrade );
        this.app.use( controllerLoginVoter );
    }

    public getRoutes(): any {
        return this.app;
    }
}