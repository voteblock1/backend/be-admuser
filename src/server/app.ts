import express = require('express');
import Routes from './routes/index.route';
import Middlewares from './middlewares/index.middleware';

export default class App {

    public app: express.Application;
    public port: number;
    public middlewareApp: Middlewares;
    public routeApp: Routes;

    constructor(port: number) {
        this.port = port;
        this.app = express();
        this.middlewareApp = new Middlewares();
        this.routeApp = new Routes();
    }

    static init(port: number) {
        return new App( port );
    }


    private loadRoutes() {
        this.app.use( '/admuser',this.routeApp.getRoutes() );
    }

    private loadMiddleware() {
        this.app.use( this.middlewareApp.getMiddleware() );
    }

    private configHeader() {
        this.app.use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method, tokenApp, tokenUser');
            res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
            res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
            next();
        });
    }

    start(callback: Function) {
        this.app.listen( this.port, callback() );
        this.configHeader();
        this.loadMiddleware();
        this.loadRoutes();
    }
}