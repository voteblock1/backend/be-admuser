import { environment } from '../../environments/environment';
import jwt = require('jsonwebtoken');

export default class Authentication {

    constructor()
    {
        
    }

    /**
     * @description Build a variable for valid the token.
     * @date 07/10/2020
     * @author scespedes
     * @version 1.0.0
     */
    public buildCheckToken(): any {
        return (req: any, res: any, next: any) => {
            let token = req.get('tokenApp');
            jwt.verify(token, environment.SEED_APP , (err: any, decoded: any) => {
              if(err){
                return res.status(401).json({
                    ok: false,
                    message: 'Full authentication is required to access this resource',
                    unauthorized: err
                });
              }
              
              req.application = decoded.application;
              next();
            });
        };
    }

    /**
     * @description Valid superUser rol.
     * @date 18/10/2020
     * @author scespedes
     * @version 1.0.0
     */
    public checkSuperUserRol(): any {
      return (req: any, res: any, next: any) => {
        let token = req.get('tokenUser');
        jwt.verify(token, environment.SEED_USER , (err: any, decoded: any) => {
          if(err){
            return res.status(401).json({
                ok: false,
                message: 'Full authentication is required to access this resource',
                unauthorized: err
            });
          }
          if(decoded.user.idTypeUser.name === 'SuperAdministrador') {
            next();
          } else {
              return res.json(
                  {
                    ok: false,
                    err: {
                      message: 'The rol is not an SuperAdministrador'
                    }
                  }
                );
          }
        });
      }
    }
}