import { Router, Request, Response } from 'express';
import _ = require('underscore');
import Company from '../models/company.model';
import Authentication from '../middlewares/authentication.middleware';
const authentication = new Authentication();
const CCompany = Router();
import Multer from '../classes/Multer.class';
const multer = new Multer();
import { environment } from '../../environments/environment';

/**
 * @description Method for creating company.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CCompany.post( '/company', [ authentication.buildCheckToken(), authentication.checkSuperUserRol() ], ( req: Request, res: Response ) => {
    let body = req.body;

    let company = new Company({
        name: body.name,
        description: body.description
    });
    company.save((err, companyDB) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            company: companyDB
        });
    });
});

/**
 * @description Method to get all company.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CCompany.get( '/company', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 15;
    limit = Number(limit);

    Company.find({state: true}/*, 'email'*/)
            .skip(from)
            .limit(limit)
            .exec((err, company) => {
                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                Company.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        company,
                        howMany: count
                    });
                });
            });
});

/**
 * @description Method to company by id.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CCompany.get( '/company/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;
    if(id == null)
    {
        return res.status(404).json({message: "Error company doesn't exist"});
    }

        Company.findById(id, (err, company) => {

            if(err) {return res.status(500).json({message: 'Error returning data'});}

            if(! company ) return res.status(404).json({message: "Error company doesn't exist"});

            return res.status(200).json({
                ok: true,
                company
            });

        });
});

/**
 * @description Method that updates the attributes of a company.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CCompany.put( '/company/:id', [ authentication.buildCheckToken(), authentication.checkSuperUserRol() ], ( req: Request, res: Response ) => {
    let id = req.params.id;
    let body =_.pick( req.body, ['name','description']);

    Company.findByIdAndUpdate( id, body, {new: true, runValidators: false}, (err, companyDB) => {

        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(! companyDB) return res.status(404).json({message: "Error company doesn't exist"});

        res.json({
            ok: true,
            company: companyDB
        });
    });
});

/**
 * @description Method that delete a company.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CCompany.delete( '/company/:id', [ authentication.buildCheckToken(), authentication.checkSuperUserRol() ], ( req: Request, res: Response ) => {

    let id = req.params.id;

    let changeState = {
        state: false
    };
    Company.findByIdAndUpdate( id, changeState, {new: true}, (err, companyDelete) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(companyDelete === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'company not found'
                    }
                });
            }
            res.json({
                ok: true,
                company: companyDelete
            });
    });
});

/**
 * @description Method that upload images for companies.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CCompany.post( '/company/upload-image/:id', [ authentication.buildCheckToken(), multer.saveImageServer('company').single('image'), authentication.checkSuperUserRol() ], ( req: Request, res: Response ) => {
    let id = req.params.id;
    let pathImage = req.file.path.split('public/');

    let uploadImage = {
        image: `${environment.HOST}${pathImage[1]}`
    };
    Company.findByIdAndUpdate( id, uploadImage, {new: true}, (err, companyDB) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(companyDB === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'company not found'
                    }
                });
            }
            res.json({
                ok: true,
                company: companyDB
            });
    });
});


/**
 * @description Method to get company by name.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CCompany.post( '/company/name', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let body = req.body;
    Company.findOne({name: body.name}, (err, company) => {

            if(err) { return res.status(500).json({message: 'Error returning data'});}

            if(! company ) return res.status(404).json({message: "Error company doesn't exist"});

            return res.status(200).json({
                ok: true,
                company
            });
        });
});

export default CCompany;