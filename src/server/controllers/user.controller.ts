import { Router, Request, Response } from 'express';
import _ = require('underscore');
import User from '../models/user.model';
import Token from '../models/token.model';
import bcrypt = require('bcrypt');
import Authentication from '../middlewares/authentication.middleware';
const authentication = new Authentication();
const CUser = Router();
import nodemailer = require('nodemailer');
var crypto = require('crypto');
import { environment } from '../../environments/environment';
import TokenEmail from '../assets/emails/tokenEmail';
const tokenEmail = new TokenEmail();
import Multer from '../classes/Multer.class';
const multer = new Multer();


var smtpTransport = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: environment.EMAIL_ADDRESS, 
        pass: environment.EMAIL_PASSWORD
    }
});

/**
 * @description Method for creating users.
 * @date 12/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CUser.post('/user', authentication.buildCheckToken(), function ( req: Request, res: Response ) {
    let body = req.body;
    
    let user = new User({
        firstName: body.firstName,
        lastName: body.lastName,
        email: body.email,
        password: bcrypt.hashSync(body.password, 15),
        idTypeUser: body.idTypeUser,
        idCompany: body.idCompany
    });
    user.save((err, userDB: any) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        let token = new Token({
            userId: userDB._id,
            token:  crypto.randomBytes(3).toString('hex'),
            idTypeToken: body.idTypeToken
        });

        token.save((err, tokenDB: any) => {
            if(err){
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            let mailOptions = tokenEmail.setSendEmailAccountVerification(userDB.firstName,userDB.email,tokenDB.token);
            smtpTransport.sendMail(mailOptions, function (err: any) {
                if (err) { return res.status(500).send({ msg: err.message }); }
                
                res.status(200).json(
                    {
                        ok: true,
                        message: 'A verification email has been sent to ' + userDB.email + '.',
                        user: userDB
                    }
                );
            });
        });
    });
});

/**
 * @description Method to get all users.
 * @date 12/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CUser.get('/user', authentication.buildCheckToken(), ( req: Request, res: Response ) => {

    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 15;
    limit = Number(limit);

    User.find({state: true})
            .skip(from)
            .limit(limit)
            .populate('idCompany', 'name')
            .populate('idTypeUser', 'name')
            .exec((err, user) => {
                if(err) {
                    return res.status(400).json({
                        ok: false,
                        err
                    });
                }

                User.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        user,
                        howMany: count
                    });
                });
            });
});

/**
 * @description Method to get users by id.
 * @date 12/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CUser.get('/user/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;
    if(id == null)
    {
        return res.status(404).json({message: "Error User doesn't exist"});
    }
    User.findById(id)
        .populate('idCompany', 'name')
        .populate('idTypeUser', 'name')
        .exec((err, user) => {
        if(err) { return res.status(500).json({message: 'Error returning data'});}

        if(! user) return res.status(404).json({message: "Error User doesn't exist"});
        
        return res.status(200).json({
            ok: true,
            user
        });

    });
});

/**
 * @description Method that updates the attributes of a user.
 * @date 12/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CUser.put('/user/:id', authentication.buildCheckToken(), function( req: Request, res: Response ){
    let id = req.params.id;
    let body =_.pick( req.body, ['firstName','lastName', 'email', 'idTypeUser']);
    if (JSON.stringify(body) === JSON.stringify({})) {
        return res.json({
            ok: false,
            err: 'No attribute was updated'
        });
    } else {
                
        User.findByIdAndUpdate( id, body, {new: true, runValidators: true}, (err, userDB) => {

            if(err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                user: userDB
            });
        });
    }
} );

/**
 * @description Method that delete a user.
 * @date 12/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CUser.delete('/user/:id', authentication.buildCheckToken(), function ( req: Request, res: Response ) {
    let id = req.params.id;

    let changeState = {
        state: false,
        email: ''
    };
    User.findByIdAndUpdate( id, changeState, {new: true}, (err, userDelete) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(userDelete === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'User not found'
                    }
                });
            }
            res.json({
                ok: true,
                user: userDelete
            });
    });
});

/**
 * @description Method to get users by email.
 * @date 12/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CUser.post('/user/search/email', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let body = req.body;

    User.find({ email: body.email })
        .exec((err, user)=>{

            if(err) { return res.status(500).json({message: 'Error returning data'});}

            if(user.length > 0){

                return res.status(200).json({
                    ok: true
                });

            }
            return res.status(200).json({
                ok: false
            });

        })
});

/**
 * @description Method to reset password.
 * @date 12/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CUser.put('/reset/password', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let token = req.body.token;

    let body = _.pick( req.body, ['password']);
    let bodyPassword = {
        password: bcrypt.hashSync(body.password, 15),
    }
    
    Token.findOne({token: token, state: true})
        .exec((err, tokenDB: any)=>{

            if(err) { return res.status(500).json({message: 'Error returning data'});}
            
            if(! tokenDB) return res.status(404).json({message: "Error Token doesn't exist"});

            User.findByIdAndUpdate(tokenDB.userId, bodyPassword, (err, userDB)=>{
                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                tokenDB.state = false;
                tokenDB.save((err: any)=>{
                    if(err){
                        return res.status(500).json({
                            ok: false,    
                            message: err
                        });
                    }
                    res.status(200).json({
                        ok: true,
                        message: 'Reset password successfully'
                    });
                });
            });
        });
});

/**
 * @description Method that upload images for users.
 * @date 14/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CUser.post( '/user/upload-image/:id', [ 
    /*authentication.buildCheckToken(), */
    multer.saveImageServer('user').single('image'),
    /*authentication.buildCheckAdminRole()*/ ], ( req: Request, res: Response ) => {
    let id = req.params.id;
    let pathImage = req.file.path.split('public/');

    let uploadImage = {
        image: `${environment.HOST}${pathImage[1]}`
    };
    User.findByIdAndUpdate( id, uploadImage, {new: true}, (err, userDB) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(userDB === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'company not found'
                    }
                });
            }
            res.json({
                ok: true,
                company: userDB
            });
    });
});


/**
 * @description Method to reset password.
 * @date 12/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CUser.put('/user/change/password', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.body.id;

    let body = _.pick( req.body, ['password']);
    let bodyPassword = {
        password: bcrypt.hashSync(body.password, 15),
    }
    
    User.findByIdAndUpdate(id, bodyPassword, (err, userDB)=>{
        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(userDB === null){
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'User not found'
                }
            });
        }

        res.json({
            ok: true,
            user: userDB,
            message: 'Contraseña actualizada'
        });
    });
});
export default CUser;