import { Router, Request, Response } from 'express';
import _ = require('underscore');
import Application from '../models/application.model';
import Authentication from '../middlewares/authentication.middleware';
const authentication = new Authentication();
const CApplication = Router();

/**
 * @description Method for creating application.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CApplication.post( '/application', [ authentication.buildCheckToken(), authentication.checkSuperUserRol() ], ( req: Request, res: Response ) => {
    let body = req.body;

    let application = new Application({
        name: body.name,
        clientSecret: body.clientSecret,
        idCompany: body.idCompany,
        idTypeUser: body.idTypeUser
    });
    application.save((err, applicationDB) => {
        if(err){
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            application: applicationDB
        });
    });
});

/**
 * @description Method to get all application.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CApplication.get( '/application', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 15;
    limit = Number(limit);

    Application.find({state: true}/*, 'email'*/)
            .skip(from)
            .limit(limit)
            .populate('idCompany', 'name')
            .populate('idTypeUser', 'name')
            .exec((err, application) => {
                if(err) {
                    return res.status(400).json({
                        ok: false,
                        err
                    });
                }

                Application.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        application,
                        howMany: count
                    });
                });
            });
});

/**
 * @description Method to application by id.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CApplication.get( '/application/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;
    if(id == null)
    {
        return res.status(404).json({message: "Error application doesn't exist"});
    }

    Application.findById(id, (err, application) => {

            if(err) { return res.status(500).json({message: 'Error returning data'});}

            if(! application ) return res.status(404).json({message: "Error application doesn't exist"});

            return res.status(200).json({
                ok: true,
                application
            });
        });
});

/**
 * @description Method that updates the attributes of a application.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CApplication.put( '/application/:id', [ authentication.buildCheckToken(), authentication.checkSuperUserRol() ], ( req: Request, res: Response ) => {
    let id = req.params.id;
    let body =_.pick( req.body, ['name','clientSecret','idCompany', 'idTypeUser']);

    Application.findByIdAndUpdate( id, body, {new: true, runValidators: false}, (err, applicationDB) => {

        if(err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if(! applicationDB) return res.status(404).json({message: "Error application doesn't exist"});

        res.json({
            ok: true,
            application: applicationDB
        });
    });
});

/**
 * @description Method that delete a application.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CApplication.delete( '/application/:id', [ authentication.buildCheckToken(), authentication.checkSuperUserRol() ], ( req: Request, res: Response ) => {
    let id = req.params.id;

    let changeState = {
        state: false
    };
    Application.findByIdAndUpdate( id, changeState, {new: true}, (err, applicationDelete) => {
            if(err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }
            if(applicationDelete === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'application not found'
                    }
                });
            }
            res.json({
                ok: true,
                application: applicationDelete
            });
    });
});

/**
 * @description Method to application by name.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CApplication.post( '/application/name', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let body = req.body;
    Application.findOne({name: body.name}, (err, application) => {

            if(err) { return res.status(500).json({message: 'Error returning data'});}

            if(! application ) return res.status(404).json({message: "Error application doesn't exist"});

            return res.status(200).json({
                ok: true,
                application
            });
        });
});

export default CApplication;