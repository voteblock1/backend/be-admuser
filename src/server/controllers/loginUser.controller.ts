import { Router, Request, Response } from 'express';
import bcrypt = require('bcrypt');
import jwt = require('jsonwebtoken');
import User from '../models/user.model';
import Authentication from '../middlewares/authentication.middleware';
const authentication = new Authentication();
import { environment } from '../../environments/environment';
const CLogin = Router();

/**
 * @description Method for loging user.
 * @date 14/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CLogin.post( '/loginUser', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let body = req.body;

    User.findOne({email: body.email})
    .populate('idTypeUser', 'name')
    .populate('idCompany', 'name')
    .exec( (err, userDB: any)=> {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(!userDB){
            return res.status(401).json({
                ok: false,
                err: {
                    message: 'unauthorized'
                }
            });
        }

       if(!bcrypt.compareSync(body.password, userDB.password))
       {
            return res.status(401).json({
                ok: false,
                err: {
                    message: 'unauthorized'
                }
            });
       }
       if(!userDB.checkEmail) {
        return res.status(401).json({
            ok: false,
            err: {
                type: 'not-verified',
                message: 'unauthorized, Your account has not been verified.'
            }
        });
       }
       let token = jwt.sign({
           user: userDB
       }, environment.SEED_USER , {expiresIn: environment.EXPIRATION_TOKEN_USER});
       res.json({
            ok: true,
            user: userDB,
            token
        });
    });

});

export default CLogin;