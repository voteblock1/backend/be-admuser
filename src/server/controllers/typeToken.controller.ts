import { Router, Request, Response } from 'express';
import _ = require('underscore');
import Authentication from '../middlewares/authentication.middleware';
import TypeToken from '../models/typeToken.model';
const CTypeToken = Router();
const authentication = new Authentication();


/**
 * @description Method for creating typeToken.
 * @date 13/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeToken.post( '/typeToken', [ authentication.buildCheckToken(), authentication.checkSuperUserRol() ], ( req: Request, res: Response ) => {
    let body = req.body;

    let typeToken = new TypeToken({
        name: body.name
    });
    typeToken.save((err, typeTokenDB) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            typeToken: typeTokenDB
        });
    });
});


/**
 * @description Method to get all typeToken.
 * @date 13/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeToken.get('/typeToken', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 15;
    limit = Number(limit);

    TypeToken.find({state: true})
            .skip(from)
            .limit(limit)
            .exec((err, typeToken) => {
                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                TypeToken.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        typeToken,
                        howMany: count
                    });
                });
            });
});

/**
 * @description Method to get typeToken by id.
 * @date 13/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeToken.get('/typeToken/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;
    if(id == null)
    {
        return res.status(404).json({message: "Error Type Token doesn't exist"});
    }

        TypeToken.findById(id, (err, typeToken) => {

            if(err) {return res.status(500).json({message: 'Error returning data'});}

            if(! typeToken) return res.status(404).json({message: "Error Type Token doesn't exist"});

            return res.status(200).json({
                ok: true,
                typeToken
            });
        });
});

/**
 * @description Method that updates the attributes of a typeToken.
 * @date 13/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeToken.put('/typeToken/:id', [ authentication.buildCheckToken(), authentication.checkSuperUserRol() ], function( req: Request, res: Response ){
    let id = req.params.id;
    let body =_.pick( req.body, ['name']);

    TypeToken.findByIdAndUpdate( id, body, {new: true, runValidators: false}, (err, typeTokenDB) => {

        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(! typeTokenDB) return res.status(404).json({message: "Error typeToken doesn't exist"});

        res.json({
            ok: true,
            user: typeTokenDB
        });
    });
});

/**
 * @description Method that delete a typeToken.
 * @date 13/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeToken.delete('/typeToken/:id', [ authentication.buildCheckToken(), authentication.checkSuperUserRol() ], function ( req: Request, res: Response ) {
    let id = req.params.id;

    let changeState = {
        state: false
    };
    TypeToken.findByIdAndUpdate( id, changeState, {new: true}, (err, typeTokenDelete) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(typeTokenDelete === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'TypeToken not found'
                    }
                });
            }
            res.json({
                ok: true,
                user: typeTokenDelete
            });
    });
});


/**
 * @description Method to get typeToken by name.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeToken.post( '/typeToken/name', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let body = req.body;
    TypeToken.findOne({name: body.name}, (err, typeToken) => {

            if(err) { return res.status(500).json({message: 'Error returning data'});}

            if(! typeToken ) return res.status(404).json({message: "Error typeToken doesn't exist"});

            return res.status(200).json({
                ok: true,
                typeToken
            });
        });
});

export default CTypeToken;

