import { Router, Request, Response } from 'express';
import bcrypt = require('bcrypt');
import jwt = require('jsonwebtoken');
import Voter from '../models/voter.model';
import Authentication from '../middlewares/authentication.middleware';
const authentication = new Authentication();
import { environment } from '../../environments/environment';
const CLogin = Router();

/**
 * @description Method for loging voter.
 * @date 14/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CLogin.post( '/loginVoter', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let body = req.body;

    Voter.findOne({userId: body.userId})
    .populate('userId', ['firstName', 'lastName'])
    .populate('gradeId', 'name')
    .exec( (err, voterDB: any)=> {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(!voterDB){
            return res.status(401).json({
                ok: false,
                err: {
                    message: 'unauthorized'
                }
            });
        }

       if(!bcrypt.compareSync(body.codeID, voterDB.codeID))
       {
            return res.status(401).json({
                ok: false,
                err: {
                    message: 'unauthorized'
                }
            });
       }
       res.json({
            ok: true,
            user: voterDB
        });
    });

});

export default CLogin;