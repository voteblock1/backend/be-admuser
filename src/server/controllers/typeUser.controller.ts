import { Router, Request, Response } from 'express';
import TypeUser from '../models/typeUser.model';
import _ = require('underscore');
import Authentication from '../middlewares/authentication.middleware';
const authentication = new Authentication();
const CTypeUser = Router();

/**
 * @description Method for creating typeUsers.
 * @date 12/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeUser.post( '/typeUser', [ authentication.buildCheckToken(), authentication.checkSuperUserRol() ], ( req: Request, res: Response ) => {
    let body = req.body;

    let typeUsers = new TypeUser({
        name: body.name,
        description: body.description
    });
    typeUsers.save((err, typeUserDB) => {
        if(err){
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            typeUser: typeUserDB
        });
    });
});

/**
 * @description Method to get all typeUsers.
 * @date 12/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeUser.get('/typeUser', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 15;
    limit = Number(limit);

    TypeUser.find({state: true}/*, 'email'*/)
            .skip(from)
            .limit(limit)
            .exec((err, typeUser) => {
                if(err) {
                    return res.status(400).json({
                        ok: false,
                        err
                    });
                }

                TypeUser.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        typeUser,
                        howMany: count
                    });
                });
            });
});

/**
 * @description Method to get typeUsers by id.
 * @date 12/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeUser.get('/typeUser/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;
    if(id == null)
    {
        return res.status(404).json({message: "Error User Type doesn't exist"});
    }

        TypeUser.findById(id, (err, typeUser) => {

            if(err) {return res.status(500).json({message: 'Error returning data'});}

            if(! typeUser) return res.status(404).json({message: "Error User Type doesn't exist"});

            return res.status(200).json({
                ok: true,
                typeUser
            });

        });
});

/**
 * @description Method that updates the attributes of a typeUsers.
 * @date 12/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeUser.put('/typeUser/:id', [ authentication.buildCheckToken(), authentication.checkSuperUserRol() ], function( req: Request, res: Response ){

    let id = req.params.id;
    let body =_.pick( req.body, ['name','description']);

    TypeUser.findByIdAndUpdate( id, body, {new: true, runValidators: false}, (err, typeUserDB) => {

        if(err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if(! typeUserDB) return res.status(404).json({message: "Error typeUser doesn't exist"});

        res.json({
            ok: true,
            user: typeUserDB
        });
    });
} );

/**
 * @description Method that delete a typeUser.
 * @date 12/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeUser.delete('/typeUser/:id', [ authentication.buildCheckToken(), authentication.checkSuperUserRol() ], function ( req: Request, res: Response ) {

    let id = req.params.id;

    let changeState = {
        state: false
    };
    TypeUser.findByIdAndUpdate( id, changeState, {new: true}, (err, typeUserDelete) => {
            if(err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }
            if(typeUserDelete === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'TypeUser not found'
                    }
                });
            }
            res.json({
                ok: true,
                user: typeUserDelete
            });
    });
});


/**
 * @description Method to get typeUser by name.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CTypeUser.post( '/typeUser/name', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let body = req.body;
    TypeUser.findOne({name: body.name}, (err, typeUser) => {

            if(err) { return res.status(500).json({message: 'Error returning data'});}

            if(! typeUser ) return res.status(404).json({message: "Error typeUser doesn't exist"});

            return res.status(200).json({
                ok: true,
                typeUser
            });
        });
});

export default CTypeUser;