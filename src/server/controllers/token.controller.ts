import { Router, Request, Response } from 'express';
import User from '../models/user.model';
import Token from '../models/token.model';
import Authentication from '../middlewares/authentication.middleware';
import { environment } from '../../environments/environment';
import ResetPassword from '../assets/emails/resetPassword';
import nodemailer = require('nodemailer');
import TokenEmail from '../assets/emails/tokenEmail';
const resetPassword = new ResetPassword();
var crypto = require('crypto');
const authentication = new Authentication();
const CToken = Router();
const tokenEmail = new TokenEmail();

var smtpTransport = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: environment.EMAIL_ADDRESS, 
        pass: environment.EMAIL_PASSWORD
    }
});

/**
 * @description Method to confirmation of email.
 * @date 13/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CToken.post('/confirmation/email', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let body = req.body;

    Token.findOne({ token: body.token, idTypeToken: body.idTypeToken })
         .exec(( err, tokenDB: any )=>{ 
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }

            if (!tokenDB) 
            { 
                return res.status(400).json(
                    { 
                        ok: false,
                        type: 'not-verified', 
                        message: 'We were unable to find a valid token. Your token my have expired.' 
                    }
                );
            }

            // If we found a token, find a matching user
            User.findOne({ _id: tokenDB.userId, email: body.email }, function ( err, userDB: any ) {

                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                if (!userDB) {
                        return res.status(400).json(
                            { 
                                ok: false,
                                message: 'We were unable to find a user for this token.' 
                            }
                        );
                    }

                if (userDB.checkEmail) { 
                        return res.status(400).json(
                            { 
                                ok: false,
                                type: 'already-verified', 
                                message: 'This user has already been verified.' 
                            }
                        );
                    }
 
                // Verify and save the user
                userDB.checkEmail = true;
                userDB.save((err: any) => {
                    if (err) { 
                        return res.status(500).json(
                            { 
                                ok: false,
                                message: err 
                            }
                        ); 
                    }
                    res.status(200).json(
                        {
                            ok: true,
                            message: "The account has been verified. Please log in."
                        }
                    );
                });
        });
    });
});

/**
 * @description Method to forgot password users by email.
 * @date 13/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CToken.post('/forget/password/email', authentication.buildCheckToken(), ( req: Request, res: Response ) => {

    let body = req.body;

    User.findOne({ email: body.email})
        .exec((err, userDB: any)=>{

            if(err) { return res.status(500).json({message: 'Error returning data'});}
            
            if(! userDB) return res.status(404).json({message: "Error User doesn't exist"});

            if(userDB.checkEmail){

                let token = new Token({
                    userId: userDB._id,
                    token:  crypto.randomBytes(3).toString('hex'),
                    idTypeToken: body.idTypeToken
                });
    
                token.save((err, tokenDB: any)=>{
                    if(err){
                        return res.status(500).json({
                            ok: false,
                            err
                        });
                    }
                    let mailOptions = resetPassword.setSendEmailResetPassword(userDB.firstName, userDB.email, tokenDB.token);
                    smtpTransport.sendMail(mailOptions, function (err) {
                        if (err) { return res.status(500).send({ msg: err.message }); }
                        
                        res.status(200).json(
                            {
                                ok: true,
                                message: 'A reset password has been sent to ' + userDB.email + '.'
                            });
                        });
                });
            }else{
                return res.status(400).json(
                    { 
                        ok: false,
                        type: 'have no been verified', 
                        message: 'This user has no been verified.' 
                    }
                );
            }
        });
    });

/**
 * @description Method to resend email.
 * @date 13/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CToken.post('/resend/email', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let body = req.body;

    User.findOne({ email: body.email })
        .exec(( err, userDB: any ) => { 

            if (err) { 
                return res.status(500).json(
                    { 
                        ok: false,
                        message: err 
                    }
                ); 
            }

            if (!userDB) { 
                return res.status(400).json(
                    { 
                        ok: false,
                        message: 'We were unable to find a user with that email.' 
                    }
                ); 
            }

            if (userDB.checkEmail) {
                return res.status(400).json(
                    { 
                        ok: false,
                        message: 'This account has already been verified. Please log in.' 
                    }
                );
            }
 
        // Create a verification token, save it, and send email
        let token = new Token({
            userId: userDB._id,
            token:  crypto.randomBytes(3).toString('hex'),
            idTypeToken: body.idTypeToken
        });
 
        // Save the token
        token.save((err, tokenDB: any) => {
            if (err) { return res.status(500).json({ message: err }); }
 
            // Send the email
            let mailOptions = tokenEmail.setSendEmailAccountVerification(userDB.firstName,userDB.email,tokenDB.token);
            smtpTransport.sendMail(mailOptions, function (err) {
                if (err) { return res.status(500).json({ message: err }); }

                res.status(200).json({
                    ok: true,
                    message: 'A verification email has been sent to ' + userDB.email + '.'
                });
            });
        });
 
    });
});

export default CToken;