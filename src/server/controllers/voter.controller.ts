import { Router, Request, Response } from 'express';
import _ = require('underscore');
import bcrypt = require('bcrypt');
import Authentication from '../middlewares/authentication.middleware';
import Voter from '../models/voter.model';
const CVoter = Router();
const authentication = new Authentication();


/**
 * @description Method for creating voter.
 * @date 14/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVoter.post( '/voter', [ authentication.buildCheckToken(), /*authentication.buildCheckAdminRole()*/ ], ( req: Request, res: Response ) => {
    let body = req.body;

    let voter = new Voter({
        gradeId: body.gradeId,
        codeStudent: body.codeStudent,
        codeID: bcrypt.hashSync(body.codeID, 10),
        userId: body.userId
    });
    voter.save((err, voterDB) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            voter: voterDB
        });
    });
});


/**
 * @description Method to get all voter.
 * @date 14/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVoter.get('/voter', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 15;
    limit = Number(limit);

    Voter.find({state: true})
            .skip(from)
            .limit(limit)
            .populate('userId', ['firstName', 'lastName'])
            .populate('gradeId', 'name')
            .exec((err, voterDB) => {
                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                Voter.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        voterDB,
                        howMany: count
                    });
                });
            });
});

/**
 * @description Method to get voter by id.
 * @date 14/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVoter.get('/voter/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;
    if(id == null)
    {
        return res.status(404).json({message: "Error Type voter doesn't exist"});
    }

        Voter.findById(id)
            .populate('userId', ['firstName', 'lastName'])
            .populate('gradeId', 'name')
            .exec( (err, voterDB) => {

                if(err) {return res.status(500).json({message: 'Error returning data'});}

                if(! voterDB) return res.status(404).json({message: "Error voter doesn't exist"});

                return res.status(200).json({
                    ok: true,
                    voterDB
                });
            });
});

/**
 * @description Method that updates the attributes of a voter.
 * @date 14/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVoter.put('/voter/:id', [ authentication.buildCheckToken(), /*authentication.buildCheckAdminRole()*/ ], function( req: Request, res: Response ){
    let id = req.params.id;
    let body =_.pick( req.body, ['grade','codeStudent']);

    Voter.findByIdAndUpdate( id, body, {new: true, runValidators: false}, (err, voterDB) => {

        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(! voterDB) return res.status(404).json({message: "Error voter doesn't exist"});

        res.json({
            ok: true,
            voter: voterDB
        });
    });
});

/**
 * @description Method that delete a voter.
 * @date 14/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CVoter.delete('/voter/:id', [ authentication.buildCheckToken(), /*authentication.buildCheckAdminRole()*/ ], function ( req: Request, res: Response ) {
    let id = req.params.id;

    let changeState = {
        state: false
    };
    Voter.findByIdAndUpdate( id, changeState, {new: true}, (err, voterDelete) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(voterDelete === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'Voter not found'
                    }
                });
            }
            res.json({
                ok: true,
                voter: voterDelete
            });
    });
});

export default CVoter;
