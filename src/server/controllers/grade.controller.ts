import { Router, Request, Response } from 'express';
import _ = require('underscore');
import Authentication from '../middlewares/authentication.middleware';
import Grade from '../models/grade.model';
const CGrade = Router();
const authentication = new Authentication();


/**
 * @description Method for creating grade.
 * @date 14/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CGrade.post( '/grade', [ authentication.buildCheckToken(), /*authentication.buildCheckAdminRole()*/ ], ( req: Request, res: Response ) => {
    let body = req.body;

    let voter = new Grade({
        name: body.name
    });
    voter.save((err, gradeDB) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            typeToken: gradeDB
        });
    });
});


/**
 * @description Method to get all grade.
 * @date 14/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CGrade.get('/grade', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 15;
    limit = Number(limit);

    Grade.find({state: true})
            .skip(from)
            .limit(limit)
            .exec((err, gradeDB) => {
                if(err) {
                    return res.status(500).json({
                        ok: false,
                        err
                    });
                }

                Grade.countDocuments({state: true}, (err, count) => {
                    res.json({
                        ok: true,
                        gradeDB,
                        howMany: count
                    });
                });
            });
});

/**
 * @description Method to get grade by id.
 * @date 14/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CGrade.get('/grade/:id', authentication.buildCheckToken(), ( req: Request, res: Response ) => {
    let id = req.params.id;
    if(id == null)
    {
        return res.status(404).json({message: "Error Type grade doesn't exist"});
    }

        Grade.findById(id, (err, gradeDB) => {

            if(err) {return res.status(500).json({message: 'Error returning data'});}

            if(! gradeDB) return res.status(404).json({message: "Error grade doesn't exist"});

            return res.status(200).json({
                ok: true,
                gradeDB
            });
        });
});

/**
 * @description Method that updates the attributes of a voter.
 * @date 14/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CGrade.put('/grade/:id', [ authentication.buildCheckToken(), /*authentication.buildCheckAdminRole()*/ ], function( req: Request, res: Response ){
    let id = req.params.id;
    let body =_.pick( req.body, ['name']);

    Grade.findByIdAndUpdate( id, body, {new: true, runValidators: false}, (err, gradeDB) => {

        if(err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if(! gradeDB) return res.status(404).json({message: "Error grade doesn't exist"});

        res.json({
            ok: true,
            user: gradeDB
        });
    });
});

/**
 * @description Method that delete a grade.
 * @date 14/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CGrade.delete('/grade/:id', [ authentication.buildCheckToken(), /*authentication.buildCheckAdminRole()*/ ], function ( req: Request, res: Response ) {
    let id = req.params.id;

    let changeState = {
        state: false
    };
    Grade.findByIdAndUpdate( id, changeState, {new: true}, (err, gradeDelete) => {
            if(err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            if(gradeDelete === null){
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'Grade not found'
                    }
                });
            }
            res.json({
                ok: true,
                user: gradeDelete
            });
    });
});

export default CGrade;
