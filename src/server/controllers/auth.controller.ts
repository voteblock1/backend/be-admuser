import { Router, Request, Response } from 'express';
import bcrypt = require('bcrypt');
import jwt = require('jsonwebtoken');
import Application from '../models/application.model';
import { environment } from '../../environments/environment';
import Encryption from '../classes/Encryption.class';
const encryption = new Encryption();
const CLoginApplication = Router();

/**
 * @description Method for loging application.
 * @date 08/10/2020
 * @author sCespedes
 * @version 1.0.0
 */
CLoginApplication.post( '/auth', ( req: Request, res: Response ) => {
    let encryptionHeader = req.get('Authorization');
    //console.log(encryption.getEncryptedAES({id: "5f866b9413d5930085c188ed", clientSecret: "94982a85-358d-4edf-8c50-9b65f4c43249"}));

    if (encryptionHeader) {
        let body = encryption.getDecryptedAES(encryptionHeader);
        Application.findById(body.id)
        .populate('idTypeUser', 'name')
        .populate('idCompany', 'name')
        .exec( (err, applicationDB: any)=> {
            if(err){
                return res.status(500).json({
                    ok: false,
                    err
                });
            }

            if(!applicationDB){
                return res.status(401).json({
                    ok: false,
                    err: {
                        message: 'unauthorized'
                    }
                });
            }

            if (applicationDB.state && body.clientSecret === applicationDB.clientSecret) {
                let token = jwt.sign({
                    application: applicationDB
                }, environment.SEED_APP , {expiresIn: environment.EXPIRATION_TOKEN_APP});
                res.json({
                        ok: true,
                        application: applicationDB,
                        token
                    });
            } else {
                return res.status(401).json({
                    ok: false,
                    err: {
                        message: 'unauthorized'
                    }
                });
            }
        });
    } else {
        return res.status(401).json({
            ok: false,
            err: {
                message: 'unauthorized'
            }
        });
    }
});




export default CLoginApplication;