import { environment } from '../../environments/environment';
import CryptoJS from "crypto-js";

export default class Encryption {

    constructor(){

    }

    /**
     * @description This method encrypts data.
     * @date 07/10/2020
     * @author scespedesg
     * @param {*} message
     */
    public getEncryptedAES(message: Object): string{
        let encrypted = CryptoJS.AES.encrypt(JSON.stringify(message), environment.ENCRYPTION_KEY).toString();

        return encrypted;
    }

    /**
     * @description This method decrypted data.
     * @date 07/10/2020
     * @author scespedesg
     * @param {*} message 
     */
    public getDecryptedAES(message: any): any{

        let authorizationToken: any = [];
        authorizationToken = message?.split(" ",2);
        let decrypted = CryptoJS.AES.decrypt(authorizationToken[1], environment.ENCRYPTION_KEY);

        return JSON.parse(decrypted.toString(CryptoJS.enc.Utf8))
    }
}