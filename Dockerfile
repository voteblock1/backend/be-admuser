############################################################
# Dockerfile para configurar aplicación en node.js - Express
############################################################

# Establece la imagen base
FROM node:12

# Crear directorio de trabajo
RUN mkdir -p /usr/src/app

# Se estable el directorio de trabajo
WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm","run","serve"]